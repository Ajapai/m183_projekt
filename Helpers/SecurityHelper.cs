﻿using m183_projekt.Models;
using m183_projekt.Models.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace m183_projekt.Helpers
{
    public static class SecurityHelper
    {
        public static string Escaping(string str)
        {
            return Regex.Replace(str, @"[\x00'""\b\n\r\t\cZ\\%_]",
                delegate (Match match)
                {
                    string v = match.Value;
                    switch (v)
                    {
                        case "\x00":            // ASCII NUL (0x00) character
                    return "\\0";
                        case "\b":              // BACKSPACE character
                    return "\\b";
                        case "\n":              // NEWLINE (linefeed) character
                    return "\\n";
                        case "\r":              // CARRIAGE RETURN character
                    return "\\r";
                        case "\t":              // TAB
                    return "\\t";
                        case "\u001A":          // Ctrl-Z
                    return "\\Z";
                        default:
                            return "\\" + v;
                    }
                });
        }

        // Creates a token that can be used once within 5 minutes to get access to certain api functions
        public static string CreateToken(ApplicationDbContext _dbContext, ApplicationUser user)
        {
            ApiToken token = new ApiToken { Id = Guid.NewGuid(), UserId = user.Id, Expiration = DateTime.Now.AddMinutes(5) };
            _dbContext.ApiTokens.Add(token);
            _dbContext.SaveChanges();
            return token.Id.ToString();
        }

        // retruns
        // null - Token is invalid
        // false - user is no Admin
        // true - user is Admin
        public static bool? CheckTokenAdmin(string ApiTokenId, ApplicationDbContext _dbContext)
        {
            ApiToken token = _dbContext.ApiTokens.FirstOrDefault(t => t.Id == new Guid(ApiTokenId));
            if (token?.IsActive??false)
            {
                ApplicationUser user = _dbContext.Users.FirstOrDefault(u => u.Id == token.UserId);
                string adminRoleId = _dbContext.Roles.First(r => r.Name == "Admin").Id;
                return user?.Roles.Any(r => r.RoleId == adminRoleId);
            }
            return null;
        }
    }
}