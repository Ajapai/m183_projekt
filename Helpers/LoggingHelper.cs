﻿using log4net;
using m183_projekt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace m183_projekt.Helpers
{
    public static class LoggingHelper
    {
        // This is to keep track of the amount of logs in the past
        private static List<DateTime> _deniedApiRequests = new List<DateTime>();
        private static Dictionary<string, List<DateTime>> _userLockouts = new Dictionary<string, List<DateTime>>();
        private static Dictionary<string, List<DateTime>> _userDataChanges = new Dictionary<string, List<DateTime>>();

        // This is to keep track of the warnings so the logfile won't get spammed
        private static Dictionary<string, List<DateTime>> _logWarnings = new Dictionary<string, List<DateTime>>();

        // Checks if there is a critical amount of logs
        private static bool IsCritical(List<DateTime> list, int criticalAmount, int minutes)
        {
            // Updates list to be all incidents in the last x minutes
            foreach (DateTime item in list.Where(x => x.AddMinutes(minutes) < DateTime.Now).ToList()) { list.Remove(item); }
            list.Add(DateTime.Now);
            return list.Count >= criticalAmount;
        }

        // Reduces the spamming of a specific warn message to once a minute (or really any time you want)
        private static void Warn(Type type, string key, string message)
        {
            if (!_logWarnings.ContainsKey(key))
            {
                _logWarnings[key] = new List<DateTime>();
            }
            if (!IsCritical(_logWarnings[key], 2, 1))
            {
                LogManager.GetLogger(type).Warn(message);
            }
        }

        // I only need Info and Warn but more could be implemented
        public static void DeniedApiRequest(Type type)
        {
            LogManager.GetLogger(type).Info("Invalid api token");
            
            // Checks if the api has failed a request at least 100 times in the last 3 minutes
            if (IsCritical(_deniedApiRequests, 5, 3))
            {
                Warn(type, "DeniedApiRequest", "Way to many failed api requests");
            }
        }

        public static void UserLockout(Type type, ApplicationUser user)
        {
            LogManager.GetLogger(type).Info($"{user.Email} is locked out. Id: {user.Id}");
            if(!_userLockouts.ContainsKey(user.Id))
            {
                _userLockouts[user.Id] = new List<DateTime>();
            }

            // Checks if user has had at least 3 lockouts in the last hour
            if (IsCritical(_userLockouts[user.Id], 3, 60))
            {
                Warn(type, $"UserLockout_{user.Id}", $"{user.Email} has a lot of login requests even thou it is locked out. Id: {user.Id}");
            }
        }

        public static void UserDataChange(Type type, ApplicationUser user)
        {
            LogManager.GetLogger(type).Info($"{user.Email} changed his data. Id: {user.Id}");
            if (!_userDataChanges.ContainsKey(user.Id))
            {
                _userDataChanges[user.Id] = new List<DateTime>();
            }

            // Checks if user has changed his data at least 5 times in the last 24 hours
            if (IsCritical(_userDataChanges[user.Id], 5, 1440))
            {
                Warn(type, $"UserDataChange_{user.Id}", $"{user.Email} has changed his data quied a lot recently. Id: {user.Id}");
            }
        }
    }
}