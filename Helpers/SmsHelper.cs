﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace m183_projekt.Helpers
{
    public static class SmsHelper
    {
        private static string Convert(string phoneNumber)
        {
            phoneNumber = phoneNumber.Replace(" ", "").Replace("+", "");
            if (phoneNumber[0] == '0')
            {
                phoneNumber = phoneNumber.Replace("0", "41");
            }
            return phoneNumber;
        }

        public static HttpClient GetSmsClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://m183.gibz-informatik.ch/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("X-Api-Key", "OQAwADQANwA3ADkANAA5ADMANAAwADcAOQA5ADgANwA0ADAA");
            return client;
        }

        public static HttpRequestMessage CreateRequest(string phoneNumber, string message)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "api/sms/message");
            request.Content = new StringContent("{\"mobileNumber\":\"" + Convert(phoneNumber) + "\",\"message\":\"" + message + "\"}",
                                                Encoding.UTF8,
                                                "application/json");
            return request;
        }
    }
}