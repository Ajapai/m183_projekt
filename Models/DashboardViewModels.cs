﻿using m183_projekt.Models.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace m183_projekt.Models
{
    public class DashboardViewModel
    {
        public ApplicationUser User { get; set; }
        public List<Post> Posts { get; set; }
        public bool ShowName { get; set; }

    }

    public class PostViewModel
    {
        public string CurrentUserId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsOwnPost { get => CurrentUserId == Post.AuthorId; }
        public bool CanEdit { get; set; }
        public Post Post { get; set; }
        public Guid PostId { get => Post.Id; }
        public State State { get => Post.State; }
        public bool IsHidden { get => State == State.hidden; }
        public bool IsDeleted { get => State == State.deleted; }
        public string AuthorName { get => Post.AuthorName; }
        public string Title { get => Post.Title; }
        public string Content { get => Post.Content; }
        public List<Comment> Comments { get => Post.Comments; }
        [Required(ErrorMessage = "Please enter a comment")]
        [StringLength(200, ErrorMessage = "Maximum 200 Characters")]
        [Display(Name ="New Comment")]
        public string NewComment { get; set; }
    }
    public class CreatePostViewModel
    {
        public string AuthorId { get; set; }
        [Required(ErrorMessage = "Please enter a title.")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Please enter some text.")]
        public string Content { get; set; }
    }

    public class CommentViewModel
    {
        public string CurrentUserId { get; set; }
        public Guid PostId { get; set; }
        [Required(ErrorMessage = "Please enter a comment")]
        [StringLength(200, ErrorMessage = "Maximum 200 Characters")]
        public string NewComment { get; set; }
    }

    public class ToggleViewModel
    {
        public Guid PostId { get; set; }
    }

    public class PublishWith2FAViewModel
    {
        public Guid PostId { get; set; }
        [Required(ErrorMessage = "Please enter the a Code")]
        [Display(Name = "Enter your code from Google Authenticator App: ")]
        public string Code2FA { get; set; }
        public string QrCodeImageUrl { get; set; }
        public bool ShowError { get; set; }
    }
}