﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace m183_projekt.Models.Classes
{
    public class Comment
    {
        public Guid Id { get; set; }
        public Guid PostId { get; set; }
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
        [MaxLength(200)]
        public string Content { get; set; }
        public DateTime Timestamp { get; set; }
    }
}