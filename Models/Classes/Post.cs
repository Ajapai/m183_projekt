﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace m183_projekt.Models.Classes
{
    public class Post
    {
        public Guid Id { get; set; }
        public State State { get; set; }
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public DateTime Timestamp { get; set; }
    }
    public enum State
    {
        hidden,
        published,
        deleted
    }
}