﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace m183_projekt.Models.Classes
{
    public class UserGoogle2FA
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public bool HasGoogleAuthenticate { get; set; }
        public string SecretKey { get; set; }
    }
}