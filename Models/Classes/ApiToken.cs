﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace m183_projekt.Models.Classes
{
    public class ApiToken
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public DateTime Expiration { get; set; }
        public bool WasUsed { get; set; }
        public bool IsActive { get => !WasUsed && Expiration < DateTime.Now; }
    }
}