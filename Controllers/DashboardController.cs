﻿using m183_projekt.Helpers;
using m183_projekt.Models;
using m183_projekt.Models.Classes;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Google.Authenticator;

namespace m183_projekt.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationDbContext _dbContext = new ApplicationDbContext();
        private ApplicationUser CurrentUser { get => UserManager.FindById(User.Identity.GetUserId()); }
        private bool CurrentUserIsAdmin { get => CurrentUser?.Roles.Any(ur => ur.RoleId == _dbContext.Roles.First(r => r.Name == "Admin").Id)??false;}

        public DashboardController()
        {
        }

        public DashboardController(ApplicationUserManager userManager, ApplicationDbContext dbContext)
        {
            _userManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Dashboard
        public ActionResult Index()
        {
            List<Post> posts = _dbContext.Posts.OrderByDescending(p => p.Timestamp).ToList();
            bool isAdmin = true;
            if(!CurrentUserIsAdmin)
            {
                posts = posts.Where(p => p.AuthorId.ToString() == CurrentUser.Id && p.State != State.deleted).ToList();
                isAdmin = false;
            }

            DashboardViewModel viewModel = new DashboardViewModel { User = CurrentUser, Posts = posts, ShowName = isAdmin };
            ViewBag.IsAdmin = isAdmin;
            ViewBag.Title = "Dashboard";
            return View(viewModel);
        }

        public ActionResult DeletedPosts()
        {
            if (!CurrentUserIsAdmin)
            {
                return RedirectToAction("Index", "Dashboard");
            }
            List<Post> posts = _dbContext.Posts.Where(p => p.State == State.deleted).OrderByDescending(p => p.Timestamp).ToList();
            DashboardViewModel viewModel = new DashboardViewModel { User = CurrentUser, Posts = posts, ShowName = true };
            ViewBag.DeletedOnly = true;
            ViewBag.Title = "Deleted Posts";
            return View("Index", viewModel);
        }

        // GET: Dashboard/Post/{Id}
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Post(Guid id)
        {
            string userId = CurrentUser?.Id ?? "no_user";
            Post post = _dbContext.Posts.FirstOrDefault(p => p.Id == id &&
                (CurrentUserIsAdmin ||
                p.State == State.published ||
                (p.AuthorId.ToString() == userId && p.State != State.deleted))
            );
            if(post != null)
            {
                post.Comments = _dbContext.Comments.Where(c => c.PostId == post.Id).OrderBy(c => c.Timestamp).ToList();
            }
            ViewBag.Title = "Post";
            return View(new PostViewModel { Post = post, CurrentUserId = userId, IsAdmin = CurrentUserIsAdmin });
        }

        // POST: Dashboard/NewComment
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewComment(CommentViewModel viewModel)
        {
            if(!ModelState.IsValid)
            {
                string userId = CurrentUser?.Id ?? "no_user";
                Post post = _dbContext.Posts.FirstOrDefault(p => p.Id == viewModel.PostId &&
                    (CurrentUserIsAdmin ||
                    p.State == State.published ||
                    (p.AuthorId.ToString() == userId && p.State != State.deleted))
                );
                post.Comments = _dbContext.Comments.Where(c => c.PostId == post.Id).OrderBy(c => c.Timestamp).ToList();
                ViewBag.Title = "Post";
                return View($"Post", new PostViewModel { Post = post, CurrentUserId = userId, IsAdmin = CurrentUserIsAdmin });
            }
            if(CurrentUser.Id != viewModel.CurrentUserId)
            {
                return RedirectToAction("Index", "Dashboard");
            }
            Comment newComment = new Comment 
            { 
                AuthorId = CurrentUser.Id, 
                AuthorName = CurrentUser.UserName, 
                PostId = viewModel.PostId, 
                Content = SecurityHelper.Escaping(viewModel.NewComment), 
                Id = Guid.NewGuid(),
                Timestamp = DateTime.Now
            };
            _dbContext.Comments.Add(newComment);
            _dbContext.SaveChanges();
            return RedirectToAction($"Post/{viewModel.PostId}", "Dashboard");
        }

        // GET: Dashboard/NewPost
        [HttpGet]
        public ActionResult NewPost()
        {
            ViewBag.Title = "New Post";
            return View(new CreatePostViewModel { AuthorId = CurrentUser.Id });
        }

        // POST: Dashboard/NewPost
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewPost(CreatePostViewModel viewModel)
        {
            if(!ModelState.IsValid)
            {
                ViewBag.Title = "New Post";
                return View(viewModel);
            }
            
            string authorName = CurrentUser.UserName;
            _dbContext.Posts.Add(
                new Post 
                { 
                    AuthorId = viewModel.AuthorId, 
                    AuthorName = authorName, 
                    Title = SecurityHelper.Escaping(viewModel.Title), 
                    Content = SecurityHelper.Escaping(viewModel.Content), 
                    Id = Guid.NewGuid(), 
                    Timestamp = DateTime.Now }
                );
            _dbContext.SaveChanges();
            
            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ToggleState(ToggleViewModel viewModel)
        {
            Post post = _dbContext.Posts.First(p => p.Id == viewModel.PostId);
            switch (post.State)
            {
                case State.hidden:
                    if(CurrentUserIsAdmin)
                    {
                        UserGoogle2FA user2FA = _dbContext.UserGoogle2FAs.FirstOrDefault(x => x.UserId == CurrentUser.Id);
                        PublishWith2FAViewModel publishViewModel = new PublishWith2FAViewModel { PostId = viewModel.PostId, QrCodeImageUrl = null };
                        if (user2FA == null)
                        {
                            user2FA = new UserGoogle2FA { UserId = CurrentUser.Id };
                            _dbContext.UserGoogle2FAs.Add(user2FA);
                            _dbContext.SaveChanges();
                        }
                        if (!user2FA.HasGoogleAuthenticate)
                        {
                            Random random = new Random();
                            user2FA.SecretKey = new string(Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", 10)
                                .Select(s => s[random.Next(s.Length)]).ToArray());
                            _dbContext.SaveChanges();

                            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                            var setupInfo = tfa.GenerateSetupCode("M183_Projekt", "Elias@Gemperle.com", user2FA.SecretKey, false, 5);
                            publishViewModel.QrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                        }
                        ViewBag.Title = "Publish";
                        return View("Publish", publishViewModel);
                    }
                    post.State = State.published;
                    _dbContext.SaveChanges();
                    break;
                case State.published:
                    post.State = State.hidden;
                    _dbContext.SaveChanges();
                    break;
                case State.deleted:
                default:
                    return RedirectToAction($"Index", "Dashboard");
            }
            return RedirectToAction($"Post/{viewModel.PostId}", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ToggleDeletion(ToggleViewModel viewModel)
        {
            Post post = _dbContext.Posts.First(p => p.Id == viewModel.PostId);
            switch (post.State)
            {
                case State.hidden:
                    post.State = State.deleted;
                    _dbContext.SaveChanges();
                    break;
                case State.published:
                    post.State = State.deleted;
                    _dbContext.SaveChanges();
                    break;
                case State.deleted:
                    post.State = State.hidden;
                    _dbContext.SaveChanges();
                    return RedirectToAction($"Post/{viewModel.PostId}", "Dashboard");
            }
            return RedirectToAction($"Index", "Dashboard");
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Publish(PublishWith2FAViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Title = "Publish";
                return View(viewModel);
            }
            UserGoogle2FA user2FA = _dbContext.UserGoogle2FAs.FirstOrDefault(x => x.UserId == CurrentUser.Id);
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            if (tfa.ValidateTwoFactorPIN(user2FA.SecretKey, viewModel.Code2FA))
            {
                if (!user2FA.HasGoogleAuthenticate)
                {
                    user2FA.HasGoogleAuthenticate = true;
                }
                Post post = _dbContext.Posts.First(p => p.Id == viewModel.PostId);
                post.State = State.published;
                _dbContext.SaveChanges();
                return RedirectToAction($"Post/{viewModel.PostId}", "Dashboard");
            }
            viewModel.ShowError = true;
            ViewBag.Title = "Publish";
            return View(viewModel);
        }
    }
}