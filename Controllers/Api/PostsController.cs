﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using m183_projekt.Helpers;
using m183_projekt.Models;
using m183_projekt.Models.Classes;
using Newtonsoft.Json;

namespace m183_projekt.Controllers.Api
{
    public class PostsController : ApiController
    {
        private ApplicationDbContext _dbContext = new ApplicationDbContext();

        [HttpGet]
        public string GetPublishedPosts()
        {
            return JsonConvert.SerializeObject(_dbContext.Posts.Where(p => p.State == State.published).ToList());
        }
        
        // I don't use this api in my project but there is a tokenAuthentification anyways...
        [HttpPost]
        public string PostDeletedPosts(string ApiTokenId)
        {
            switch (SecurityHelper.CheckTokenAdmin(ApiTokenId, _dbContext))
            {
                // Is Admin
                case true:
                    return JsonConvert.SerializeObject(_dbContext.Posts.Where(p => p.State == State.deleted).ToList());
                // Is not Admin
                case false:
                // No user
                default:
                    LoggingHelper.DeniedApiRequest(typeof(PostsController));
                    return "No Access";
            }
        }
    }
}
