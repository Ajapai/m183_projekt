﻿using log4net;
using m183_projekt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace m183_projekt.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            DashboardViewModel viewModel = new DashboardViewModel();
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                viewModel.Posts = dbContext.Posts.Where(p => p.State == Models.Classes.State.published).OrderByDescending(p => p.Timestamp).ToList();
            }
            viewModel.ShowName = true;
            ViewBag.Title = "Home";
            return View("../Dashboard/Index", viewModel);
        }
    }
}