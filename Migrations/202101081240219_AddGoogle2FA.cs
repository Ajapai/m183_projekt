namespace m183_projekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGoogle2FA : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserGoogle2FA",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(),
                        HasGoogleAuthenticate = c.Boolean(nullable: false),
                        SecretKey = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserGoogle2FA");
        }
    }
}
