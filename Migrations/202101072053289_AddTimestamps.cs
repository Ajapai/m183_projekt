namespace m183_projekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTimestamps : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "Timestamp", c => c.DateTime(nullable: false));
            AddColumn("dbo.Posts", "Timestamp", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "Timestamp");
            DropColumn("dbo.Comments", "Timestamp");
        }
    }
}
