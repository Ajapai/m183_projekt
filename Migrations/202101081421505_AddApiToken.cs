namespace m183_projekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApiToken : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApiTokens",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(),
                        Expiration = c.DateTime(nullable: false),
                        WasUsed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ApiTokens");
        }
    }
}
