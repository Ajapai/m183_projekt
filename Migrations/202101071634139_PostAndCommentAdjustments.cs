namespace m183_projekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostAndCommentAdjustments : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "Author_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Posts", "Author_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Comments", new[] { "Author_Id" });
            DropIndex("dbo.Posts", new[] { "Author_Id" });
            AddColumn("dbo.Comments", "AuthorName", c => c.String());
            AddColumn("dbo.Posts", "AuthorName", c => c.String());
            AlterColumn("dbo.Comments", "AuthorId", c => c.String());
            AlterColumn("dbo.Posts", "AuthorId", c => c.String());
            DropColumn("dbo.Comments", "Author_Id");
            DropColumn("dbo.Posts", "Author_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "Author_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Comments", "Author_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Posts", "AuthorId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Comments", "AuthorId", c => c.Guid(nullable: false));
            DropColumn("dbo.Posts", "AuthorName");
            DropColumn("dbo.Comments", "AuthorName");
            CreateIndex("dbo.Posts", "Author_Id");
            CreateIndex("dbo.Comments", "Author_Id");
            AddForeignKey("dbo.Posts", "Author_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Comments", "Author_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
