﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(m183_projekt.Startup))]
namespace m183_projekt
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
